/*@TODO: Include the OpenMP header file */

#include <iostream>
#include <sstream>

int main(int argc, char *argv[])
{

    /* @TODO: Insert OpenMP pragma to make the following code block parallel */
    {
        std::stringstream ss;

        /*
         * @TODO: Get the individual thread number in 'id'
         * using a runtime routine
         */
        int id = 0;

        /* Print Hello world from each thread */
        ss << "Hello World from thread "<< id << std::endl;
        std::cout << ss.str();
        ss.str("");

        /*
         * @TODO: Make absolutely sure each thread has printed "Hello World",
         * before we move forward
         */

        /* @TODO: Now get the master thread to identify itself! */
        ss << "Hi, I am MASTER, my id is always " << id << std::endl;
        std::cout << ss.str();
        ss.str("");

        /*
         * Now print Hello OpenMP from each thread
         * (after all everybody needs to learn OpenMP!)
         */
        ss << "Hello OpenMP from thread " << id << std::endl;
        std::cout << ss.str();
    }

    return 0;
}
