
/*@TODO: Include the OpenMP header file */
#include <stdio.h>

int main(int argc, char *argv[])
{

    /* @TODO: Insert OpenMP pragma to make the following code block parallel */
    {
        /* 
         * @TODO: Get the individual thread number in 'id'
         * using a runtime routine 
         */
        int id = 0; 
        
        /* Print Hello world from each thread */
        printf("Hello World from thread %d\n", id);

        /* 
         * @TODO: Make absolutely sure each thread has printed "Hello World",
         * before we move forward 
         */
        
        /* @TODO: Now get the master thread to identify itself! */
        printf("Hi, I am MASTER, my id is always %d\n", id);
        
        
        /* 
         * Now print Hello OpenMP from each thread 
         * (after all everybody needs to learn OpenMP!)
         */
        printf("Hello OpenMP from thread %d\n", id);

    }

    return 0;
}
