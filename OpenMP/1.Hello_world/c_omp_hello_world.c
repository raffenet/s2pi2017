/* Including the OpenMP header file */
#include <omp.h>

#include <stdio.h>

int main(int argc, char *argv[])
{
    #pragma omp parallel
    {
        /* 
         * Getting the individual thread number in 'id'
         * using a runtime routine 
         */
        int id = omp_get_thread_num();
        
        /* Print Hello world from each thread */
        printf("Hello World from thread %d\n", id);

        /* 
         * Making sure each thread has printed "Hello World",
         * before we move forward 
         */
        #pragma omp barrier
        
        /* Making the master thread to identify itself! */
        #pragma omp master
            printf("Hi, I am MASTER, my id is always %d\n", id);
        
        
        /* 
         * Now printing Hello OpenMP from each thread
         * (after all everybody needs to learn OpenMP!)
         */
        printf("Hello OpenMP from thread %d\n", id);

    }

    return 0;
}
